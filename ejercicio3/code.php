<?php

$retorno = array();
$cost_per_minute = [0.2, 0.35, 0.4, 0.45];
$cost_per_mile = [1.1, 1.8, 2.3, 3.5];

$time_start = microtime_float();
function fareEstimator($ride_time, $ride_distance, $cost_per_minute, $cost_per_mile) {
    foreach ($cost_per_minute as $keyminute => $valueminute) {
        $price1 = ($ride_time * $valueminute);
        $price1 += ($ride_distance * $cost_per_mile[$keyminute]);
        $retorno[] = floatval(number_format($price1, 2, '.', ' '));
    }
    $time_start = microtime_float();
    $time_total = $time_end - $time_start;
    return $retorno;
}

?>

<div class="row">
	<div class="col-md-12">
        <h2> RETORNO </h2>
        <span>
            <?php
                echo json_encode(fareEstimator(30, 7, $cost_per_minute, $cost_per_mile));
            ?>
        <span>
    </div>
    <div class="col-md-12">
        <h2> Tiempo total  </h2>
        <ul>
            <li><?php echo $time_total; ?></li>
        <ul>
    </div>
</div>
