<?php

$retorno = array();
$domains = [
    "en.wiki.org",
    "codefights.com",
    "happy.net",
    "code.info"
];

$valida = true;
if(4 < count($domains) && count($domains) > 25)
        $valida = false;
$time_start = microtime_float();
foreach ($domains as $key => $value) {
    if(5 < strlen($value) && strlen($value) > 20){
        $valida = false;
    }
    if($valida){
        $domain = parse_url('http://'.$value, PHP_URL_HOST);
        $domainArray = explode('.', $domain);
        $retorno[] = $domainArray[count($domainArray)-2];
    }
}
$time_start = microtime_float();
$time_total = $time_end - $time_start;
?>

<div class="row">
	<div class="col-md-12">
        <h2> Valores Input </h2>
        <ul>
            <?php
                foreach($domains as $valor){
            ?>
                <li><?php echo $valor; ?></li>
            <?php
                }
            ?>
        <ul>
        <hr/>
    </div>
    <div class="col-md-12">
        <h2> Valores Retorno  </h2>
        <ul>
            <?php
                foreach($retorno as $valor){
            ?>
                <li><?php echo $valor; ?></li>
            <?php
                }
            ?>
        <ul>
    </div>
    <div class="col-md-12">
        <h2> Tiempo total  </h2>
        <ul>
            <li><?php echo $time_total; ?></li>
        <ul>
    </div>
</div>
